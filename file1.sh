    c=3                                                     # Number of tries
    b="$((1 + $RANDOM % 99))"                               # Randomiser
    while [ $c -gt 0 ]; do      
        echo "Let us guess a number! You have $c tries!"
        read NUM
        if [[ $NUM -eq $b ]]; then                          # Comparasion between input and generated number.           
    
           echo "Congratulations! You won!"                 # The program ends if they are equal
    
        exit 1
        
        elif [[ $NUM -gt $b ]]; then                        # If numbers are not equal then program offers to make a guess again
            echo "$NUM is a bit wrong! It is greater than generated number! Try again"
        else 
            echo "$NUM is a bit wrong! It is lesser than generated number! Try again"
        fi                                                  # End of conditional
    c=$((c - 1))                                            # Cycle goes on until c is greater than 0
    done                                                    
    echo "Oh well. The correct number was $b"
